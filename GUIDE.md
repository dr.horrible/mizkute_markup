# Небольшой гайд по верстке основных элементов

## Кнопки

```pug
.button.button_theme_green.button_size_small
  .button__text Кнопка
```


### Создадим файл `src/blocks/_button.sсss`

По умолчанию создаем класс `.button` у которого будут базовые стили для кнопок, высота, шрифт и т.п.

```sсss
.button {
  height: 60px;
  border: none;
  outline: none;
  cursor: pointer;
  font-size: 16px;
  font-weight: 700;
  padding: 0 30px;
  user-select: none;
  transition: .3s ease-out;

  &__text {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
}
```

Если кнопка это ссылка то дополнительно ниже пропишем inline-block

```sсss
a.button {
  display: inline-block;
  text-decoration: none;
}
```

Если нам нужно сделать кнопку другого цвета, добавляем модификатор к этому классу
```scss
.button {
  &_theme_green {
    background-color: green;
  }

  &_theme_red
    background-color: red;
  }
}
```

Если нам нужно сделать кнопку больше или меньше, добавляем модификатор к этому классу
```scss
.button {
  &_size_small {
    height: 40px;
    font-size: 14px;
  }

  &_size_large {
    height: 80px;
    font-size: 18px;
  }
}
```

## Поля формы

### Инпуты

Рекомендую придерживаться такой структуры, чтобы потом не было проблем с валидацией, <br />
ошибка будет прокидываться в конец блока `.form-field` и у нас не будет проблем если <br />
вдруг есть справа иконка которая absolute.

По умолчанию для полей не нужно прописывать стили. Лучше делать это через класс. <br />
Бывают ситуации когда поля слишком разные по стилям и если по умолчанию прописать  <br />какие-то стили, то потом придется перебивать.

```pug
.form-field
  .form-field__label Введите имя
  .form-field__input
    input(type='text' name='name' value='' required).form-input
    .form-field__icon
      +icon('user')
```
