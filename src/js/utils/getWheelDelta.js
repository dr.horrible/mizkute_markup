export default function getWheelDelta(ev, el) {
  const delta = ev.wheelDelta

  if (el) {
    el.scrollLeft -= delta * 1
  }
}
