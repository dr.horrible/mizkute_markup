import svg4everybody from 'svg4everybody'
import $ from 'jquery'
import validate from 'jquery-validation'

svg4everybody()

window.$ = $
window.jQuery = $
