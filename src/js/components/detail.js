import getWheelDelta from '../utils/getWheelDelta'

window.addEventListener('mousewheel', e => {
  if ($('.js-detail-scroll').length > 0) {
    const $scrollEl = document.querySelector('.js-detail-scroll')
    getWheelDelta(e, $scrollEl)
  }
})

let timeoutId

const animateClickButton = () => {
  if ($('.js-last-detail-item').length > 0) {
    const $lastDetailItem = $('.js-last-detail-item')
    const lastItemLeft = $lastDetailItem.offset().left
    const $button = $lastDetailItem.find('.js-anim-button')
    const time = +$button.data('time')

    if (lastItemLeft === 0) {
      setTimeout(() => {
        $button.css('transition-duration', `${time}ms`).addClass('is-load')

        if (!timeoutId) {
          timeoutId = setTimeout(() => {
            $button[0].click()
          }, time)
        }
      }, 500)
    } else {
      $button.css('transition-duration', '500ms').removeClass('is-load')
      if (timeoutId) {
        clearTimeout(timeoutId)
        timeoutId = null
      }
    }
  }
}

$('.js-detail-scroll').on('scroll', () => {
  animateClickButton()
})
