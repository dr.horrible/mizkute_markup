import parallaxInstance from './parallax'

$('.js-open-modal').on('click', () => {
  if ($(window).width() > 1024 && parallaxInstance) {
    parallaxInstance.disable()
  }
  $('.js-feedback-modal').show().addClass('is-open')
})

$('.js-close-modal').on('click', () => {
  if ($(window).width() > 1024 && parallaxInstance) {
    parallaxInstance.enable()
  }
  $('.js-feedback-modal').removeClass('is-open').delay(700).fadeOut()
})
