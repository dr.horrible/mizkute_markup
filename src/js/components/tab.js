$('.js-tab-button').on('click', e => {
  const $el = $(e.currentTarget)
  const idx = $el.index()
  const $tabs = $('.js-tabs')

  if (!$el.hasClass('is-active')) {
    $el.addClass('is-active').siblings().removeClass('is-active')
    $tabs.find('.js-tab-content').eq(idx).fadeIn(700).siblings().hide()
  }
})
