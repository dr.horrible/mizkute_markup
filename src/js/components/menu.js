$('.js-menu-open').on('click', () => {
  $('.js-menu').addClass('is-show')
  $('body').addClass('is-menu-open')
})

$('.js-menu-close').on('click', () => {
  $('.js-menu').removeClass('is-show')
  $('body').removeClass('is-menu-open')
})
