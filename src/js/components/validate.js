$('.js-validate').each((index, form) => {
  $(form).validate({
    ignore: [':disabled'],
    errorPlacement: function (error, element) {
      return true
    },
    highlight(element) {
      setTimeout(() => {
        $(element).parent().removeClass('is-success')
        $(element).parent().addClass('is-error')
      })
    },
    unhighlight(element) {
      setTimeout(() => {
        if ($(element).val() !== '' && $(element).val() !== '0')
          $(element).parent().addClass('is-success')
        $(element).parent().removeClass('is-error')
      })
    },
  })
  $('input', form).on('change', function () {
    $(this).valid()
  })
})

$.validator.addMethod('emailcustom', value =>
  value.match(
    /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9A-Яа-я_\-\.])+\.([A-Za-zA-Яа-я_]{2,6})$/
  )
)

$.validator.addClassRules('js-email', {
  emailcustom: true,
})

$.validator.addMethod('phonecustom', value => {
  return value.match(/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){11}(\s*)?$/)
})

$.validator.addClassRules('js-mob', {
  phonecustom: true,
})
