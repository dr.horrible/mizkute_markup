import IMask from 'imask'

if ($('.js-mask').length > 0) {
  const element = document.querySelector('.js-mask')
  const maskOptions = {
    mask: '+{7}(000)000-00-00',
  }

  const mask = IMask(element, maskOptions)
}
