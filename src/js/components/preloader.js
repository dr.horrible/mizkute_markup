document.fonts.ready.then(function () {
  if ($('.js-preloader-title').length > 0) {
    $('.js-preloader-title').addClass('is-show')
  }
})

$(window).on('load', () => {
  $('body').addClass('is-loaded')

  setTimeout(() => {
    if ($('.js-preloader-hide').length > 0) {
      $('.js-preloader-hide').each(function (i, elem) {
        const ew = $(elem).find('span').width()

        $(elem)
          .delay(i * 500)
          .animate({ width: `${ew}px` }, 300, () => {
            $(elem).addClass('is-show')
            if (i === $('.js-preloader-hide').length - 1) {
              $('.js-preloader').delay(700).fadeOut(700)
            }
          })
      })
    }

    if ($('.js-preloader-title').length > 0) {
      $('.js-preloader').delay(1000).fadeOut(700)
    }
  }, 300)
})
