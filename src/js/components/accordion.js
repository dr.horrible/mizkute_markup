$('.js-accordion-btn').on('click', e => {
  const $accordion = $(e.currentTarget).parents('.js-accordion')
  const $accordionContent = $accordion.find('.js-accordion-content')

  $accordion.toggleClass('is-open')
  $accordionContent.slideToggle(500)
})
