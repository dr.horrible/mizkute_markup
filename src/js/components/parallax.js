import Parallax from 'parallax-js'

let parallaxInstance

if ($('.js-parallax').length > 0) {
  const scene = document.querySelector('.js-parallax')

  if ($(window).width() > 1366) {
    parallaxInstance = new Parallax(scene, {
      relativeInput: true,
      clipRelativeInput: true,
      pointerEvents: true,
    })

    parallaxInstance.origin(0.2, 0.075)
    parallaxInstance.scalar(10, 90)
  }
}

export default parallaxInstance
