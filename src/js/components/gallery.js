import getWheelDelta from '../utils/getWheelDelta'

let galleryScrollEnd = 0

function scrollAnimations() {
  const $galleryInner = $('.js-gallery-scroll')
  const galleryScroll = $galleryInner.scrollLeft()
  const galleryScrollWidth = $galleryInner[0].scrollWidth
  const percent = galleryScroll / galleryScrollWidth
  const transVal = $('.js-scroll-el-wrap').width() * percent
  const $scrollEl = $('.js-scroll-el')
  const scrollElLeft = $scrollEl.offset().left

  let moveVal

  galleryScrollEnd < $('.js-gallery-scroll').scrollLeft()
    ? (moveVal = 5)
    : (moveVal = -5)

  $('.js-scroll-el-round').css('transform', `translateX(${moveVal}px)`)
  $scrollEl.css('transform', `translateX(${transVal}px)`)

  $('.js-gallery-item').each((idx, item) => {
    const itemLeft = $(item).offset().left
    const itemRight = $(item).offset().left + $(item).width()

    if (itemRight > scrollElLeft && itemLeft < scrollElLeft) {
      $(item).addClass('is-vis')
      idx % 2 !== 0
        ? $('body').addClass('dark-theme')
        : $('body').removeClass('dark-theme')
    } else {
      $(item).removeClass('is-vis')
    }
  })
}

window.addEventListener('mousewheel', e => {
  if ($('.js-gallery-scroll').length > 0) {
    const $scrollEl = document.querySelector('.js-gallery-scroll')

    getWheelDelta(e, $scrollEl)

    return (galleryScrollEnd = $('.js-gallery-scroll').scrollLeft())
  }
})

$('.js-gallery').on('touchend', e => {
  return (galleryScrollEnd = $('.js-gallery-scroll').scrollLeft())
})

$('.js-gallery-scroll').on('scroll', () => {
  scrollAnimations()
})
